package com.e.fragment_kotlin
import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_fragment_1.*
import kotlinx.android.synthetic.main.fragment_fragment_2.*

class Fragment_1 : Fragment()
{
    var listener:Result?=null


    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        edittext1
        edittext2
        btn1
        btn2
        btn3
           btn1.setOnClickListener{
               if (edittext1 != null && edittext2 != null) {
                   var a = edittext1.text.toString().toInt()
                   var b = edittext2.text.toString().toInt()
                   var d = a * b
                   d.toString()
                   listener?.multi(d)
               }
           }
        btn2.setOnClickListener {
            if (edittext1 != null && edittext2 != null) {
                var a = edittext1.text.toString().toInt()
                var b = edittext2.text.toString().toInt()
                var d = a + b
                listener?.add(d)
            }
        }
        btn3.setOnClickListener {
            if (edittext1 != null && edittext2 != null) {
                var a = edittext1.text.toString().toInt()
                var b = edittext2.text.toString().toInt()
                var d = a - b
                listener?.sub(d)
            }
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_fragment_1, container, false)
        return view
    }
    interface Result {
        fun add(position: Int)
        fun sub(position: Int)
        fun multi(position: Int)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is Result) {
            listener = context as Result?
        } else {
            throw RuntimeException(context!!.toString() + "implement Listener")
        }
    }
}