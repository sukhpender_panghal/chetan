package com.e.fragment_kotlin

import androidx.fragment.app.Fragment

class Communicator: Fragment() {
    lateinit var callback: OnHeadlineSelectedListener
    fun Listener(callback: OnHeadlineSelectedListener) {
        this.callback = callback
    }
    interface OnHeadlineSelectedListener {
        abstract val su: Any

        fun Selected(position: Int)
    }
}