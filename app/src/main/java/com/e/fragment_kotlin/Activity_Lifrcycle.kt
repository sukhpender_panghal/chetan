package com.e.fragment_kotlin
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
class Activity_Lifrcycle : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity__lifrcycle)
Log.d("asd","oncreate")
    }

    override fun onStart() {
        super.onStart()
Log.d("asd","onstart")
    }

    override fun onResume() {
        super.onResume()
Log.d("asd","onResume")
    }

    override fun onPause() {
        super.onPause()
Log.d("asd","onPause")
    }

    override fun onStop() {
        super.onStop()
Log.d("asd","onstop")
    }

    override fun onRestart() {
        super.onRestart()
        Log.d("asd","restart")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("TAG", "destroy")
    }
}