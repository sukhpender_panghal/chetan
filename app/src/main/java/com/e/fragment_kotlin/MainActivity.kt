package com.e.fragment_kotlin
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_blank.*
import kotlinx.android.synthetic.main.fragment_fragment_1.*
import androidx.core.app.ComponentActivity
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.util.Log
import kotlinx.android.synthetic.main.fragment_fragment_2.*
import kotlinx.android.synthetic.main.fragment_fragment_3.*
import kotlinx.android.synthetic.main.fragment_fragment_4.*


class MainActivity: AppCompatActivity(),Fragment_1.Result
{
    var d = ""
    override fun add(position: Int) {
            multiply.setText("$position")
    }

    override fun sub(position: Int) {
        minus.setText("$position")
    }

    override fun multi(position: Int) {
            add.setText("$position")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        container1
        container2
        container3
        container4

        val manager = supportFragmentManager
        val t = manager.beginTransaction()

        t.add(R.id.container1,Fragment_1())
        t.add(R.id.container2,Fragment_2())
        t.add(R.id.container3,Fragment_3())
        t.add(R.id.container4,Fragment_4())
        t.commit()
    }
}