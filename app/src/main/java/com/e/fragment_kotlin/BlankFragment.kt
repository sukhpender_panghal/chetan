package com.e.fragment_kotlin

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.util.Log
import kotlinx.android.synthetic.main.fragment_blank.*

class BlankFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d("asd", "create")

    }

    private fun Intent(blankFragment: BlankFragment, java: Class<BlankFragment2>): Intent? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_blank, container, false)
        Log.d("asd","createview")
        btn
        btn.setOnClickListener {
            val i = Intent(this@BlankFragment, BlankFragment2::class.java)
            startActivity(i)
        }
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        Log.d("asd","attached")

    }

    override fun onStart() {
        super.onStart()
        Log.d("asd","start")
    }

    override fun onStop() {
        super.onStop()
        Log.d("asd","stop")
    }

    override fun onResume() {
        super.onResume()
        Log.d("as","resume")
    }

    override fun onPause() {
        super.onPause()
        Log.d("aas","pause")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("S","DESTROY")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.d("sdd","destryview")
    }

    override fun onDetach() {
        super.onDetach()
        Log.d("asd","detach")
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    }

